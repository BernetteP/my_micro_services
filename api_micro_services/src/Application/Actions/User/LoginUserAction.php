<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\User\User;

class LoginUserAction extends UserAction
{
    protected function action(): Response
    {
        $data = $this->request->getParsedBody();
        $userName = $data["name"];
        //$userPass = password_hash($data["password"], PASSWORD_ARGON2I);
        $user = $this->user->find($userName);
        //$user->save();
        return $this->respondWithData($user);
    }
}